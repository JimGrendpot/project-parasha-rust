use tcod::colors::*;
use tcod::console::*;

use std::cmp;
use std::vec::Vec;
use rand::Rng;
use tcod::map::{FovAlgorithm, Map as FovMap};

const SCREEN_WIDTH: i32 = 80;
const SCREEN_HEIGHT: i32 = 50;
// size of the map
const MAP_WIDTH: i32 = 80;
const MAP_HEIGHT: i32 = 45;

//room_param
const MAX_ROOMS: i32 = 15;
const MIN_ROOMS: i32 = 5;

const ROOM_MAX_SIZE: i32 = 10;
const ROOM_MIN_SIZE: i32 = 5;

//colors
const COLOR_DARK_WALL: Color = Color { 
    r: 25, 
    g: 25, 
    b: 25 
};
const COLOR_DARK_GROUND: Color = Color {
    r: 25,
    g: 25,
    b: 25,
};

const COLOR_VIS_GROUND: Color = Color {
    r: 55,
    g: 55,
    b: 125,
};

const COLOR_VIS_WALL: Color = Color {
    r: 35,
    g: 35,
    b: 105,
};
const COLOR_LIGHT_WALL: Color = Color {
    r: 142,
    g: 5,
    b: 24,
};

const COLOR_LIGHT_GROUND: Color = Color {
    r: 255,
    g: 161,
    b: 0,
};

const DOOR_COLOR: Color = Color {
    r: 50,
    g: 50,
    b: 200
};

const ENEMY_PASSIVE: Color = Color {
    r: 200,
    g: 200,
    b: 200
};

const ENEMY_100: Color = Color {
    r: 142,
    g: 5,
    b: 24
};

const FOV_ALGO: FovAlgorithm = FovAlgorithm::Basic; // default FOV algorithm
const FOV_LIGHT_WALLS: bool = true; // light walls or not
const TORCH_RADIUS: i32 = 10;

#[derive(Clone, Copy, Debug)]
struct Rect {
    x1: i32,
    y1: i32,
    x2: i32,
    y2: i32,
    xm: i32, //middle x
    ym: i32, //middle y
}

impl Rect {
    pub fn new(x: i32, y: i32, w: i32, h: i32) -> Self {
        Rect {
            x1: x,
            y1: y,
            x2: x + w,
            y2: y + h,
            xm: x + w / 2,
            ym: y + h / 2
        }
    }
}
fn create_random_room(map: &mut Map) -> Rect {
    let room = Rect::new(rand::thread_rng()
                                .gen_range(0,  MAP_WIDTH - ROOM_MAX_SIZE),
                         rand::thread_rng()
                                .gen_range(0, MAP_HEIGHT - ROOM_MAX_SIZE),
                         rand::thread_rng()
                                .gen_range(ROOM_MIN_SIZE, ROOM_MAX_SIZE),
                         rand::thread_rng()
                                .gen_range(ROOM_MIN_SIZE, ROOM_MAX_SIZE));
    create_room(room, map);
    room
}

fn create_room(room: Rect, map: &mut Map) {
    // go through the tiles in the rectangle and make them passable
    for x in (room.x1 + 1)..room.x2 {
        for y in (room.y1 + 1)..room.y2 {
            map[x as usize][y as usize] = Tile::empty();
        }
    }
}

fn create_tunnel_h(x1: i32, x2: i32, y: i32, map: &mut Map) {
    for x in cmp::min(x1, x2)..(cmp::max(x1, x2) +1) {
        map[x as usize][y as usize] = Tile::empty();
    }
}

fn create_tunnel_v(y1: i32, y2: i32, x: i32, map: &mut Map) {
    for y in cmp::min(y1, y2)..(cmp::max(y1, y2) +1) {
        map[x as usize][y as usize] = Tile::empty();
    }
}

fn create_tunnel(x1: i32, x2: i32, y1: i32, y2: i32, map: &mut Map){
    if &x1 == &x2 {
        create_tunnel_v(y1, y2, x1, map);       
    }
    else if &y1 == &y2 {
        create_tunnel_h(x1, x2, y1, map);
    }
    else{
        create_tunnel_v(y1, y2, x1, map);
        create_tunnel_h(x1, x2, y2, map);
    }}

fn make_map(room_list: &mut Vec<Rect>) -> Map {
    // fill map with "blocked" tiles
    let mut map = vec![vec![Tile::wall(); MAP_HEIGHT as usize]; MAP_WIDTH as usize];

    // create two rooms
    &room_list.push(create_random_room(&mut map));

    //create_tunnel(30, 50, 25, 30, &mut map);
    
    let mut rng = rand::thread_rng();
    for i1 in 1..(rng.gen_range(MIN_ROOMS, MAX_ROOMS) as i32){
        &room_list.push(create_random_room(&mut map));
        let i:usize = i1 as usize;
        create_tunnel(room_list[i-1].xm , room_list[i].xm ,
                      room_list[i-1].ym , room_list[i].ym ,
                      &mut map);
    }
    map
}
/// A tile of the map and its properties
#[derive(Clone, Copy, Debug)]
struct Tile {
    blocked: bool,
    block_sight: bool,
    visible: bool
}

impl Tile {
    pub fn empty() -> Self {
        Tile {
            blocked: false,
            block_sight: false,
            visible: false
        }
    }

    pub fn wall() -> Self {
        Tile {
            blocked: true,
            block_sight: true,
            visible: false
        }
    }
}

type Map = Vec<Vec<Tile>>;

struct Game {
    map: Map,
}


fn render_enemys(tcod: &mut Tcod, enemys: &mut Vec<Enemy>, player: &Object){
    for enemy in enemys {
        if player.looking(tcod, enemy.x, enemy.y){
            if enemy.role == "Enemy" {
                if enemy.looking(tcod, player.x, player.y){
                    enemy.color = RED;
                }
                else {
                    enemy.color = WHITE;
                }
            }
            if player.looking(tcod, enemy.x, enemy.y){
                enemy.draw(&mut tcod.con);
            }
        };
    };
}

fn render_all(tcod: &mut Tcod, game: &mut Game, fov_recompute: bool, 
              objects: &mut Vec<Object>, enemys: &mut Vec<Enemy>, 
              player: &mut Object ) {
// go through all tiles, and set their background color
    if fov_recompute {
        tcod.fov
            .compute_fov(player.x, player.y, 
                         TORCH_RADIUS, FOV_LIGHT_WALLS, 
                         FOV_ALGO);
    }

    for y in 0..MAP_HEIGHT {
        for x in 0..MAP_WIDTH {
            let visible = tcod.fov.is_in_fov(x, y);
            let wall = game.map[x as usize][y as usize].block_sight;
            let was_vis = game.map[x as usize][y as usize].visible;

            let color = match (visible, wall, was_vis) {
                // outside of field of view:
                (false, true, false) => COLOR_DARK_WALL,
                (false, false, false) => COLOR_DARK_GROUND,
                // inside fov:
                (true, true, false)|(true, true, true) => COLOR_LIGHT_WALL,
                (true, false, false)|(true, false, true)=> COLOR_LIGHT_GROUND,

                (false, false, true) => COLOR_VIS_GROUND,
                (false, true, true) => COLOR_VIS_WALL
            };
            if visible{
                game.map[x as usize][y as usize].visible = true;
            }
            tcod.con
                .set_char_background(x, y, color, BackgroundFlag::Set);
        }
    }
    // draw all objects in the list
    player.draw(&mut tcod.con);
    for enemy in enemys {
        if player.looking(tcod, enemy.x, enemy.y){
            if enemy.role == "Enemy" {
                if enemy.looking(tcod, player.x, player.y){
                    enemy.color = ENEMY_100;
                }
                else{
                    enemy.color = ENEMY_PASSIVE;
                }
                
            }
            if player.looking(tcod, enemy.x, enemy.y){
                enemy.draw(&mut tcod.con);
            }
        }
    };

    for object in objects {
        if player.looking(tcod, object.x, object.y){
            object.draw(&mut tcod.con);
        }
    }


    blit(
        &tcod.con,
        (0, 0),
        (MAP_WIDTH, MAP_HEIGHT),
        &mut tcod.root,
        (0, 0),
        1.0,
        1.0,
    );
}

const LIMIT_FPS: i32 = 20; // 20 frames-per-second maximum



#[derive(Debug)]
struct Object {
    x: i32,
    y: i32,
    char: char,
    color: Color,
    role: String,
    view: i32,
    ai: bool,
    //character: Character, 
}

impl Object {
    pub fn new(x: i32, y: i32, char: char, color: Color, role: String, view: i32, ai: bool) -> Self {
        Object { x, y, char, color, role, view, ai }
    }

    pub fn move_by(&mut self, dx: i32, dy: i32, game: &Game) {
        if !game.map[(self.x + dx) as usize][(self.y + dy) as usize].blocked {
            self.x += dx;
            self.y += dy;
        }
    }

    pub fn draw(&self, con: &mut dyn Console) {
        con.set_default_foreground(self.color);
        con.put_char(self.x, self.y, self.char, BackgroundFlag::None);
    }

    pub fn looking(&self, tcod: &mut Tcod, x: i32, y: i32) -> bool {
        tcod.fov
            .compute_fov(self.x, self.y,
                         self.view, FOV_LIGHT_WALLS,
                         FOV_ALGO);
        
        tcod.fov.is_in_fov(x, y) 
    }

    pub fn move_or_fight(&self, tcod: &mut Tcod, dx: i32, dy:i32, game: &Game, obj: &Object) -> bool {
        if !game.map[(self.x + dx) as usize][(self.y + dy) as usize].blocked {
           println!("{} {}", self.x, self.y );
        }
        true
    }
}

struct Enemy {
    x: i32,
    y: i32,
    char: char,
    color: Color,
    role: String,
    view: i32,
    ai: bool,
}

impl Enemy {
    pub fn new(x: i32, y: i32, char: char, color: Color, role: String, view: i32, ai: bool) -> Self {
        Enemy { x, y, char, color, role, view, ai }
    }

    pub fn draw(&self, con: &mut dyn Console) {
        con.set_default_foreground(self.color);
        con.put_char(self.x, self.y, self.char, BackgroundFlag::None);
    }

    pub fn looking(&self, tcod: &mut Tcod, x: i32, y: i32) -> bool {
        tcod.fov
            .compute_fov(self.x, self.y,
                         self.view, FOV_LIGHT_WALLS,
                                           FOV_ALGO);

        tcod.fov.is_in_fov(x, y)
    }
}

struct Tcod {
    root: Root,
    con : Offscreen,
    fov: FovMap,
}

fn handle_keys(tcod: &mut Tcod, player: &mut Object, game: &Game) -> bool {
    use tcod::input::Key;
    use tcod::input::KeyCode::*;

    // todo: handle keys
    let key = tcod.root.wait_for_keypress(true);
    match key {
        Key {
            code: Enter,
            alt: true,
            ..
        } => {
            let fullscreen = tcod.root.is_fullscreen();
            tcod.root.set_fullscreen(!fullscreen);
        }
        Key { code: Escape, .. } => return true,
        Key { code: Up, .. } => if player.y != 0{
                player.move_by(0, -1, game);
            }
            else {
                player.y = SCREEN_HEIGHT;
            },
        Key { code: Down, .. } => if player.y != SCREEN_HEIGHT{
                player.move_by(0, 1, game);
            }
            else {
                player.y = 0;
            },
        Key { code: Left, .. } => player.move_by(-1, 0, game),
        Key { code: Right, .. } => player.move_by(1, 0, game),

        _ => {}
    };
    false
}
fn place_obj(room: &Rect, objects: &mut Vec<Object>, ){
    let num_objects = &objects.len();
    let mut randint = rand::thread_rng();
    if randint.gen_range(0,10) > 4{
        objects.push(Object::new(
                    randint.gen_range(room.x1 + 1, room.x2 - 1),    //x
                    randint.gen_range(room.y1 + 1, room.y2 - 1),    //y
                    '0',                                            //char
                    WHITE,                                          //color
                    "Enemy".to_string(),                            //type
                    5,                                              //view
                    true));                                         //ai
    }
}

fn place_enemy(room: &Rect, enemys: &mut Vec<Enemy>, ){
    let mut randint = rand::thread_rng();
    if randint.gen_range(0,10) > 4{
        enemys.push(Enemy::new(
                    randint.gen_range(room.x1 + 1, room.x2 - 1),    //x
                    randint.gen_range(room.y1 + 1, room.y2 - 1),    //y
                    '0',                                            //char
                    ENEMY_PASSIVE,                                  //color
                    "Enemy".to_string(),                            //type
                    5,                                              //view
                    true));                                         //ai
    }
}

fn math_enemy(objects: &mut Vec<Object>){
    for object in objects {
        if object.role == "Enemy" {
            //println!("{}:{}", object.x, object.y);
           print!(""); 
        }
    }
}

fn main(){
    let mut room_list       = Vec::new(); //list of rooms
    let mut enemys_list     = Vec::new(); //list of enemys
    let mut objects_list    = Vec::new(); //list of objects

    let root = Root::initializer()
        .font("arial12x12.png", FontLayout::Tcod)
        .font_type(FontType::Greyscale)
        .size(SCREEN_WIDTH, SCREEN_HEIGHT)
        .title("Rust/libtcod tutorial")
        .init();

    let mut tcod = Tcod { 
        root,
        con: Offscreen::new(MAP_WIDTH, MAP_HEIGHT),
        fov: FovMap::new(MAP_WIDTH, MAP_HEIGHT),
    };
    
    let mut player = Object::new(25,                    // x
                                 23,                    // y
                                 '@',                   // char
                                 BLACK,                 // color
                                 "Player".to_string(),  // model
                                 10,                    // view
                                 false);                // ai

    let mut exit = Object::new(SCREEN_WIDTH / 2,             // x
                          SCREEN_HEIGHT /2,             // y
                          'X',                          // char
                          DOOR_COLOR,                       // color
                          "Exit".to_string(),           // model
                          0,                            // view
                          false);                       // ai
    objects_list.push(exit);
    
    let mut game = Game {
        // generate map (at this point it's not drawn to the screen)
        map: make_map(&mut room_list),
    };
    let mut randint = rand::thread_rng(); 
    
    for _ in 0..room_list.len(){
        place_enemy(&room_list[randint.gen_range(1, room_list.len() - 1)],&mut enemys_list);
    }

    for y in 0..MAP_HEIGHT {
        for x in 0..MAP_WIDTH {
            tcod.fov.set(
                x,
                y,
                !game.map[x as usize][y as usize].block_sight,
                !game.map[x as usize][y as usize].blocked,
            );
        }
    }
    
    let mut previous_player_position = (-1, -1);
    
    objects_list[0].x = room_list[room_list.len() - 1].xm;
    objects_list[0].y = room_list[room_list.len() - 1].ym;

    player.x = room_list[0].xm;
    player.y = room_list[0].ym;
    
    tcod::system::set_fps(LIMIT_FPS);
    while !tcod.root.window_closed() {
        tcod.root.set_default_foreground(WHITE);
        tcod.root.clear();
        tcod.con .clear();
        
        let fov_recompute = previous_player_position != (player.x, player.y);
        
        math_enemy(&mut objects_list);

        render_all(&mut tcod, &mut game, fov_recompute, &mut objects_list, &mut enemys_list, &mut player);
        blit(
            &tcod.con,
            (0, 0),
            (SCREEN_WIDTH, SCREEN_HEIGHT),
            &mut tcod.root,
            (0, 0),
            1.0,
            1.0,
        );
       
        tcod.root.flush();
        previous_player_position = (player.x, player.y);

        let exit = handle_keys(&mut tcod, &mut player, &game);
        let mut next_l = false;
    
        if (player.x == objects_list[0].x)&(player.y == objects_list[0].y){
            next_l = true;
        }

        if exit | next_l {
            break;
        }
    }
}
